module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    "plugin:@typescript-eslint/recommended",
    "plugin:nuxt/recommended",
    "plugin:vue/vue3-essential",
    "plugin:prettier/recommended",
  ],
  parserOptions: {
    ecmaVersion: "latest",
    parser: "@typescript-eslint/parser",
    sourceType: "module",
  },
  plugins: ["@typescript-eslint"],
  rules: {
    "prettier/prettier": [
      "error",
      {
        semi: false,
        tabSize: 2,
        endOfLine: "auto",
      },
    ],
    "vue/no-ref-as-operand": "error",
    "vue/no-v-html": "off",
    "vue/no-setup-props-destructure": "off",
    "vue/multi-word-component-names": "off",
    "vue/html-self-closing": [
      "error",
      {
        html: {
          void: "always",
          normal: "any",
          component: "always",
        },
      },
    ],
    "no-unused-vars": 0,
    // 'no-undef': 0,
  },
}
