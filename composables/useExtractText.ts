// Used for extraction raw text from html string, getting from VueQuill plugin
export const useExtractContent = (string: string | null | undefined) => {
  if (string) {
    const span = document.createElement("span")
    span.innerHTML = string

    const children = span.querySelectorAll("*")
    for (let i = 0; i < children.length; i++) {
      if (children[i].textContent) children[i].textContent += " "
      else children[i].innerText += " "
    }

    return [span.textContent || span.innerText].toString().replace(/ +/g, " ")
  }

  return string
}
