export function useLoadingState() {
  const loadingCompleted = ref(false)
  const loading = ref(true)
  const nuxtApp = useNuxtApp()
  // const { showModal, hideModal } = useShowModal()
  // console.log(document)

  // App hooks =>
  nuxtApp.hook("page:start", () => {
    // console.log("page:start")

    loading.value = true
    loadingCompleted.value = false
    // set body scroll padding offset
    // showModal()
  })
  nuxtApp.hook("page:finish", () => {
    // console.log("page:finish")
    window.scrollTo(0, 0)

    loading.value = false
    setTimeout(() => {
      loadingCompleted.value = true
      // unset body scroll padding offset
      // hideModal()
    }, 450)
  })

  return { loadingCompleted, loading }
}
