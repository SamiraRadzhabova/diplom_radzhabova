export const useShowModal = () => {
  const modalFlag = ref<boolean>(false)
  const originalOverflow = ref<string>("")
  const originalPaddingRight = ref<string>("")
  const paddingOffset = ref<string>("")

  originalOverflow.value = document.documentElement.style.overflow
  originalPaddingRight.value = document.body.style.paddingRight

  function showModal() {
    modalFlag.value = true

    paddingOffset.value = window.innerWidth - document.body.offsetWidth + "px"
    document.documentElement.style.overflow = "hidden"
    document.body.style.paddingRight = paddingOffset.value
  }
  function hideModal() {
    modalFlag.value = false

    document.documentElement.style.overflow = "unset"
    document.body.style.paddingRight = originalPaddingRight.value
  }

  return { modalFlag, showModal, hideModal }
}
