import { useScreen } from "vue-screen"
const screen = useScreen()

export function useShowSearchPopup() {
  const readOnlySearch = ref(false)
  const isOpen = ref(false)

  // Methods
  function openModal(event: Event, shouldBlur: boolean) {
    isOpen.value = true

    if (screen.width < 845) {
      lockPageScroll()

      if (readOnlySearch.value) {
        readOnlySearch.value = true
      }

      if (shouldBlur && event.target instanceof HTMLInputElement) {
        event.target?.blur() // Вызываем blur(), если shouldBlur равно true
      }
    }
  }

  function closeModal() {
    isOpen.value = false

    if (screen.width < 845) unlockPageScroll()
  }

  return {
    readOnlySearch,
    closeModal,
    openModal,
    isOpen,
  }
}
