import { useFetch } from "#app"
import { useAuthStore } from "~/stores/auth"
import { useModalStore } from "~/stores/modal"
import { API_ERRORS } from "~/constants/api"

// Creating custom function for fetching data with determined BASE_URL //
type useFetchType = typeof useFetch

export const useMyFetch: useFetchType = (request, options = {}) => {
  const { accessToken, refreshToken } = toRefs(useAuthStore())
  const { openModalRegisterEmail } = useModalStore()
  const config = useRuntimeConfig()

  clearError()

  return useFetch(request, {
    baseURL: config.public.API_BASE_URL,
    headers: {
      authorization: accessToken.value ? `Bearer ${accessToken.value}` : "",
      accept: "application/json",
      "cache-control": "no-cache",
    },
    ...options,

    watch: false,
    deep: false,

    onResponseError({ response }) {
      if (response.status === API_ERRORS.UNAUTHORIZED) {
        accessToken.value = null
        refreshToken.value = null

        openModalRegisterEmail()
      }
    },
  })
}
