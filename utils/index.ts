// Debounce handler for input fields
import { IComponentValidationErrors } from "~/types";
import { IFetchError } from "~/types/api";

export function debounce<T extends []>(
  func: (...args: T) => void,
  delay: number
) {
  let timer: NodeJS.Timeout;

  return function (...args: T) {
    clearTimeout(timer);
    timer = setTimeout(() => func.apply(this, args), delay);
  };
}

export function deepMerge(target, source) {
  for (const key in source) {
    if (source.hasOwnProperty(key)) {
      if (source[key] instanceof Object) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        deepMerge(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: source[key] });
      }
    }
  }

  return target;
}

export function unlockPageScroll() {
  document.documentElement.style.overflow = "unset";
}
export function lockPageScroll() {
  document.documentElement.style.overflow = "hidden";
}

export function generateUniqId() {
  return Date.now().toString(36) + Math.random().toString(36).substr(2);
}
