import type { RouterConfig } from "@nuxt/schema"
import { resolve } from "path"
import qs, { IStringifyOptions, IParseOptions } from "qs"

const parserConfig: IParseOptions = {
  decoder: (str: string) => {
    // Преобразование строковых значений в соответствующие типы данных, если это необходимо
    if (/^\d+$/.test(str)) {
      return parseInt(str, 10)
    } else if (str === "true" || str === "false") {
      return str === "true"
    } else {
      return str
    }
  },
}
const serializerConfig: IStringifyOptions = {
  arrayFormat: "indices",
  encode: false,
}

// Initialize custom URL query parser with qs package =>
export default <RouterConfig>{
  stringifyQuery: (obj: unknown) => qs.stringify(obj, serializerConfig),
  parseQuery: (string: string) => qs.parse(string, parserConfig),
  scrollBehavior: (to) => {
    if (to.hash) {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve({
            el: to.hash,
            top: 150,
            behavior: "smooth",
          })
        }, 500)
      })
    }
  },
}
