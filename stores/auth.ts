import {
  IAuthState,
  IUserRegisterEmail,
  IUserRegisterPassword,
} from "~/types/auth"
import { IFetchError, IFetchResult, IFetchSuccess } from "~/types/api"
import { LocationQueryValue } from "#vue-router"

export const useAuthStore = defineStore("auth", {
  state: (): IAuthState => ({
    accessToken: null,
    refreshToken: null,
    regToken: null,
    userEmail: null,
  }),
  actions: {
    async registerEmail(email: string | null) {
      clearNuxtData("user-email")
      const { data, error } = await useMyFetch<
        IFetchSuccess<IUserRegisterEmail>,
        IFetchError
      >("/register/email", {
        method: "POST",
        body: { email },
        key: "user-email",
      })

      return { data: data.value, error: { ...error.value } }
    },
    async registerPassword(
      regToken: string | null,
      password: string | null,
      repeatedPassword: string | null
    ) {
      clearNuxtData("user-password")

      const { data, error } = await useMyFetch<
        IFetchSuccess<IUserRegisterPassword>,
        IFetchError
      >("/register", {
        method: "POST",
        body: { regToken, password, repeatedPassword },
        key: "user-password",
      })

      if (data.value) {
        this.accessToken = data.value.data.accessToken
      }

      return { data: data.value, error: { ...error.value } }
    },
    async authEmail(email: string | null) {
      clearNuxtData("user-auth-email")

      const { data, error } = await useMyFetch<
        IFetchSuccess<IUserRegisterPassword>,
        IFetchError
      >("/email/check", {
        method: "POST",
        body: { email },
        key: "user-auth-email",
      })

      return { data: data.value, error: { ...error.value } }
    },
    async signIn(email: string | null, password: string | null) {
      clearNuxtData("user-auth-password")

      const { data, error } = await useMyFetch<
        IFetchSuccess<IUserRegisterPassword>,
        IFetchError
      >("/login", {
        method: "POST",
        body: { email, password },
        key: "user-auth-password",
      })

      if (data.value) {
        this.accessToken = data.value.data.accessToken
        this.refreshToken = data.value.data.refreshToken
      }

      return { data: data.value, error: { ...error.value } }
    },
    async signInWithProvider(
      provider: LocationQueryValue | LocationQueryValue[],
      token: LocationQueryValue | LocationQueryValue[]
    ) {
      const { data, error } = await useMyFetch<
        IFetchSuccess<IUserRegisterPassword>,
        IFetchError
      >(`/login/${provider}`, {
        method: "POST",
        body: { token },
      })

      if (data.value) {
        const { accessToken } = data.value.data

        this.accessToken = accessToken
      }

      return { data: data.value, error: { ...error.value } }
    },
    async recoveryEmail(email: string | null) {
      clearNuxtData("reset-email")

      const { data, error } = await useMyFetch<IFetchResult, IFetchError>(
        "/password/send-reset-link",
        {
          method: "POST",
          body: { email },
          key: "reset-email",
        }
      )

      return { data: data.value, error: { ...error.value } }
    },

    async recoveryPassword(
      email: string | null,
      resetToken: string | null,
      password: string | null,
      repeatedPassword: string | null
    ) {
      clearNuxtData("new-password")

      const { data, error } = await useMyFetch<IFetchResult, IFetchError>(
        "/password/reset",
        {
          method: "POST",
          body: { email, resetToken, password, repeatedPassword },
          key: "new-password",
        }
      )
      return { data: data.value, error: { ...error.value } }
    },

    async signOut() {
      const { data, error } = await useMyFetch("/logout", {
        method: "POST",
      })

      if (data.value) {
        this.accessToken = null
        this.refreshToken = null
      }

      return { data: data.value, error: { ...error.value } }
    },
  },
  getters: {},
  persist: {
    paths: ["accessToken", "refreshToken"],
  },
})
