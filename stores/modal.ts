// Types =>
import { lockPageScroll, unlockPageScroll } from "~/utils";

export type VueComponent = InstanceType<typeof component>;
import { IModalState, IModalProps } from "~/types/modal";

// Helpers =>
import { extend } from "@vue/shared";

const component = extend({});
// const initialState = { component: null, props: {} }

export const useModalStore = defineStore("modal", {
  state: (): IModalState => ({
    component: null,
    props: {},
  }),
  actions: {
    openModal(payload: IModalProps) {
      // Block page scroll
      lockPageScroll();

      // Get props and component from payload and assign them to the state =>
      const { component, props } = payload;

      this.$state = { component, props: props || {} };
    },
    closeModal() {
      // Unlock page scroll
      unlockPageScroll();
      // Reset modal state =>
      this.component = null;
      this.props = {};
    },
  },
});
