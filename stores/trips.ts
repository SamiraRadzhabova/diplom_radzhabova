// Types =>
import {
  ITripFilters,
  ITripState,
  ITripDepartures,
  ITripDetails,
  FetchQueryIdT,
  ITripLocation,
  ITripsNearest,
  IFilterPayload,
  ISEOLinkTrips,
} from "~/types/trips"
import { ICurrencyOption, ISearchCityItem } from "~/types"
import {
  IFetchError,
  IFetchSuccess,
  IFetchSuccessPaginated,
  IFetchQuery,
} from "~/types/api"

// Modules =>
import QueryString from "qs"
import _ from "lodash"

// Constants =>
import { PROMISE_RESULT } from "~/constants/api"
import { QUERY_KEY } from "~/constants/trips"
import { ITripInfoPayload } from "~/types/booking"

export const useTripsStore = defineStore("trips", {
  state: (): ITripState => ({
    pendingTripsDetails: false,
    pendingTripsFetch: false,
    pendingTripsMore: false,
    pendingSearchBar: false,

    trips: {
      dates: [
        {
          date: { date: "", name: "" },
          price: 0,
          busDeparturesCount: 0,
          active: false,
        },
        {
          date: { date: "", name: "" },
          price: 0,
          busDeparturesCount: 0,
          active: false,
        },
        {
          date: { date: "", name: "" },
          price: 0,
          busDeparturesCount: 0,
          active: false,
        },
      ],
      departures: {
        first_page_url: null,
        next_page_url: null,
        prev_page_url: null,
        current_page: 1,
        per_page: null,
        from: null,
        path: null,
        data: [],
        to: null,
      },
      nearestDates: [],
    },
    search: {
      departureDate: "",
      fromCity: {
        searchResults: [],
        selected: null,
        query: "",
      },
      toCity: {
        searchResults: [],
        selected: null,
        query: "",
      },
      passengers: {
        adults: {
          count: 1,
        },
        children: {
          ages: [],
          count: 0,
        },
        animals: {
          weights: [],
          count: 0,
        },
        format: "",
      },
    },
    filters: {
      departureBusStops: [],
      arrivalBusStops: [],
      departureTypes: [],
      transportTypes: [],
      departureTimes: [],
      prepayments: [],
      busOptions: [],
      currencies: [],
      sortFields: [],
    },
    fetchQuery: {
      [QUERY_KEY.DEPARTURE_BUS_STOPS]: [],
      [QUERY_KEY.DEPARTURE_OWN_STOP]: undefined,
      [QUERY_KEY.ARRIVAL_BUS_STOPS]: [],
      [QUERY_KEY.DEPARTURE_TYPE_ID]: [],
      [QUERY_KEY.ARRIVAL_OWN_STOP]: undefined,
      [QUERY_KEY.DEPARTURE_TIMES]: [],
      [QUERY_KEY.TRANSPORT_TYPES]: [],
      [QUERY_KEY.DEPARTURE_DATE]: undefined,
      [QUERY_KEY.PREPAYMENTS]: [],
      [QUERY_KEY.BUS_OPTIONS]: [],
      [QUERY_KEY.TICKET_BACK]: undefined,
      [QUERY_KEY.CURRENCY_ID]: undefined,
      [QUERY_KEY.PASSENGERS]: {
        children: { count: undefined, ages: [] },
        animals: { count: undefined, weights: [] },
        adults: { count: 1 },
      },
      [QUERY_KEY.ORDER_ID]: undefined,
      [QUERY_KEY.PER_PAGE]: undefined,
      [QUERY_KEY.SORT]: undefined,
      [QUERY_KEY.FROM]: { table: undefined, id: undefined },
      [QUERY_KEY.TO]: { table: undefined, id: undefined },
      [QUERY_KEY.PAGE]: 1,
    },
    validationErrors: {
      city: {
        from: false,
        to: false,
      },
      departureDate: false,
      passengers: false,
    },
  }),
  actions: {
    async findCity(searchQuery: string) {
      this.pendingSearchBar = true

      const { data, error } = await useMyFetch<
        IFetchSuccessPaginated<ISearchCityItem>,
        IFetchError
      >("/locations", {
        query: { search: searchQuery },
      })

      this.pendingSearchBar = false
      return { data: data.value, error: error.value }
    },
    async fetchFilters() {
      const { data, error } = await useMyFetch<
        IFetchSuccess<ITripFilters>,
        IFetchError
      >("/filters")

      if (data.value) {
        this.filters = data.value.data

        // set basic sort
        if (!this.fetchQuery.sort) {
          this.fetchQuery.sort = this.filters.sortFields[0].id
        }

        // set basic currency
        if (!this.fetchQuery.currencyId) {
          const baseCurrency = this.filters.currencies.find(
            (currency) => currency.code === "UAH"
          )

          this.fetchQuery.currencyId = baseCurrency?.id
        }
      }

      return { data: data.value, error: error.value }
    },
    async fetchTrips(initialFetch = true, resetQuery = false) {
      // search validation
      if (process.client) this.validateSearch()

      if (this.hasValidationError) {
        return { data: null, error: "validation error" }
      }

      // show pertinent pending status depending on fetch type
      if (initialFetch) this.pendingTripsFetch = true
      else this.pendingTripsMore = true

      if (resetQuery) this.fetchQuery[QUERY_KEY.PAGE] = 1

      this.transformPassengers()

      const query = QueryString.stringify(this.fetchQuery)

      const { data, error } = await useMyFetch<
        IFetchSuccess<ITripDepartures>,
        IFetchError
      >(`/departures?${query}`)

      if (data.value) {
        // initialFetch flag means that the trips data will be
        // overwritten completely by fetch response, or if the flag
        // === false - just add new trips data

        if (!initialFetch) {
          // pushing new trips to existing trips
          _.mergeWith(
            this.trips.departures,
            data.value.data.departures,
            this.concatArrays
          )
          // const currentTrips = this.trips.departures.data

          // data.value.data.departures.data.forEach((trip) => {
          //   this.trips.departures.data.push(trip)
          // })
        } else {
          // overwrite the whole data structure
          this.trips = data.value.data
        }
      }

      // clean departures data to avoid inconsistency between the data and internal app state
      // in case of failed request
      if (error.value) this.resetDeparturesData()

      if (initialFetch) this.pendingTripsFetch = false
      else this.pendingTripsMore = false

      return { data: data.value, error: error.value }
    },
    async fetchSEOTrips(from: string | string[], to: string | string[]) {
      const { data, error } = await useMyFetch<
        IFetchSuccess<ISEOLinkTrips>,
        IFetchError
      >("/departures/search/cities", {
        query: { from, to },
      })

      if (data.value) {
        // desctructure response
        const { filter, data: foundedTrips } = data.value.data

        // desctructure app store values
        const {
          fromCity,
          toCity,
          departureDate: searchDepartureDate,
        } = toRefs(this.search)
        const {
          to,
          from,
          departureDate: queryDepartureDate,
        } = toRefs(this.fetchQuery)

        // save founded trips info to the app store
        this.trips = foundedTrips

        // update app search fields with founded cities data
        fromCity.value.selected = filter.from
        fromCity.value.query = `${filter.from.name}, ${filter.from.countryName}`
        to.value.id = filter.to.id
        to.value.table = filter.to.table

        toCity.value.selected = filter.to
        toCity.value.query = `${filter.to.name}, ${filter.to.countryName}`
        from.value.id = filter.from.id
        from.value.table = filter.from.table

        // update app search datepicker value
        searchDepartureDate.value = filter.departureDate
        queryDepartureDate.value = filter.departureDate
      }

      return { data: data.value, error: error.value }
    },
    async fetchTripsNearest() {
      this.pendingTripsMore = true

      this.transformPassengers()

      const query = QueryString.stringify(this.fetchQuery)

      const { data, error } = await useMyFetch<
        IFetchSuccess<ITripsNearest>,
        IFetchError
      >(`/departures/nearest?${query}`)

      if (data.value) {
        const targetObj = this.trips.nearestDates.find((nearestDateObj) => {
          return (
            nearestDateObj.date.date ===
            this.fetchQuery[QUERY_KEY.DEPARTURE_DATE]
          )
        })

        if (targetObj) {
          _.mergeWith(
            targetObj.departures,
            data.value.data.departures,
            this.concatArrays
          )
        }
      }

      this.pendingTripsMore = false
      return { data: data.value, error: error.value }
    },
    async fetchTripDetails(
      tripId: number | string | string[],
      query: ITripInfoPayload
    ) {
      this.pendingTripsDetails = true

      const { data, error } = await useMyFetch<
        IFetchSuccess<ITripDetails>,
        IFetchError
      >(`/departures/${tripId}`, { query })

      this.pendingTripsDetails = false
      return { data: data.value, error: error.value }
    },
    async getLocation(
      table: string | undefined,
      id: string | number | undefined
    ) {
      const { data, error } = await useMyFetch<
        IFetchSuccess<ITripLocation>,
        IFetchError
      >(`/locations/${table}/${id}`)

      let result = null

      if (data.value) result = data.value.data

      return { result, error: error.value }
    },
    async prefetchTrips(currentQuery: IFetchQuery) {
      const promises = []

      if (currentQuery) promises.push(this.fetchTrips())
      // fetch cities from/to if they exist in query
      if (currentQuery.from) {
        promises.push(
          this.getLocation(currentQuery.from.table, currentQuery.from.id)
        )
      }
      if (currentQuery.to) {
        promises.push(
          this.getLocation(currentQuery.to.table, currentQuery.to.id)
        )
      }

      const [trips, cityFrom, cityTo] = await Promise.allSettled(promises)

      // update search bar results with new values
      if (cityFrom.status === PROMISE_RESULT.SUCCESS) {
        const { result: city } = cityFrom.value

        // For cities => concate cityName + countryName
        // For countries => take only country name
        this.search.fromCity.selected = city
        this.search.fromCity.query = city.countryName
          ? `${city.name}, ${city.countryName}`
          : city.name

        this.validationErrors.city.from = false
      }

      if (cityTo.status === PROMISE_RESULT.SUCCESS) {
        const { result: city } = cityTo.value

        this.search.toCity.selected = city
        this.search.toCity.query = city.countryName
          ? `${city.name}, ${city.countryName}`
          : city.name

        this.validationErrors.city.to = false
      }
    },
    // helpers
    transformPassengers() {
      // reset query values to avoid duplicates
      this.fetchQuery[QUERY_KEY.PASSENGERS] = {
        children: { count: undefined, ages: [] },
        animals: { count: undefined, weights: [] },
        adults: { count: undefined },
      }

      const { fetchQuery, search } = toRefs(this)

      // if adults were added
      const adultsCount = search.value[QUERY_KEY.PASSENGERS].adults

      if (adultsCount) {
        fetchQuery.value[QUERY_KEY.PASSENGERS].adults.count =
          search.value[QUERY_KEY.PASSENGERS].adults.count
      }

      // if children were added
      const childrenCount =
        search.value[QUERY_KEY.PASSENGERS].children.ages.length

      if (childrenCount) {
        fetchQuery.value[QUERY_KEY.PASSENGERS].children.count = childrenCount
        search.value[QUERY_KEY.PASSENGERS].children.ages.forEach((child) => {
          return fetchQuery.value[QUERY_KEY.PASSENGERS].children.ages.push(
            child.age
          )
        })
      }

      // if animals were added
      const animalCount =
        search.value[QUERY_KEY.PASSENGERS].animals.weights.length

      if (animalCount) {
        fetchQuery.value[QUERY_KEY.PASSENGERS].animals.count = animalCount

        search.value[QUERY_KEY.PASSENGERS].animals.weights.forEach((animal) => {
          return fetchQuery.value[QUERY_KEY.PASSENGERS].animals.weights.push(
            animal.weight
          )
        })
      }
    },
    validateSearch() {
      this.resetSearchValidationErrors()

      const { fromCity, toCity, departureDate, passengers } = this.search

      if (!fromCity.selected) {
        this.validationErrors.city.from = true
      }
      if (!toCity.selected) {
        this.validationErrors.city.to = true
      }
      if (!departureDate) {
        this.validationErrors.departureDate = true
      }
      if (passengers.animals.weights.length) {
        const invalidWeight = passengers.animals.weights.some((weight) => {
          if (typeof weight.weight !== "number" || weight.weight == 0) {
            return true
          }
        })

        if (invalidWeight) this.validationErrors.passengers = true
      }
    },
    concatArrays(objValue: [], srcValue: []) {
      if (_.isArray(objValue)) {
        return objValue.concat(srcValue)
      }
    },
    parseUrlQuery(urlQuery: IFetchQuery) {
      // -- merging query from the URL to the app store
      _.merge(this.fetchQuery, urlQuery)

      // -- merging passengers to search bar data → transform them to inner logic state
      if (urlQuery.passengers) {
        function transformPassenger(objValue: [], srcValue: [], key: string) {
          if (key === "ages" || key === "weights") {
            // Преобразование массивов в объекты с соответствующими значениями
            return srcValue.map((value) => ({
              [key === "ages" ? "age" : "weight"]: value,
            }))
          }
        }

        _.mergeWith(
          this.search.passengers,
          urlQuery.passengers,
          transformPassenger
        )

        // set search date from query
        if (this.fetchQuery.departureDate) {
          this.search.departureDate = this.fetchQuery.departureDate
        }
      }
    },
    updateQuery(filterPayload: IFilterPayload) {
      const { filterKey, filterName, filterId } = filterPayload

      // update fetchQuery in the store
      const currentIds = this.fetchQuery[filterKey] as number[]

      if (currentIds.includes(filterId)) {
        this.fetchQuery[filterKey] = currentIds.filter((id) => id !== filterId)
      } else {
        this.fetchQuery[filterKey] = [...currentIds, filterId]
      }
    },
    resetFilters() {
      this.fetchQuery[QUERY_KEY.DEPARTURE_BUS_STOPS] = []
      this.fetchQuery[QUERY_KEY.ARRIVAL_BUS_STOPS] = []
      this.fetchQuery[QUERY_KEY.DEPARTURE_TYPE_ID] = []
      this.fetchQuery[QUERY_KEY.DEPARTURE_TIMES] = []
      this.fetchQuery[QUERY_KEY.TRANSPORT_TYPES] = []
      this.fetchQuery[QUERY_KEY.PREPAYMENTS] = []
      this.fetchQuery[QUERY_KEY.BUS_OPTIONS] = []
    },
    resetFetchQuery() {
      this.fetchQuery = {
        [QUERY_KEY.DEPARTURE_BUS_STOPS]: [],
        [QUERY_KEY.DEPARTURE_OWN_STOP]: undefined,
        [QUERY_KEY.ARRIVAL_BUS_STOPS]: [],
        [QUERY_KEY.DEPARTURE_TYPE_ID]: [],
        [QUERY_KEY.ARRIVAL_OWN_STOP]: undefined,
        [QUERY_KEY.DEPARTURE_TIMES]: [],
        [QUERY_KEY.TRANSPORT_TYPES]: [],
        [QUERY_KEY.DEPARTURE_DATE]: undefined,
        [QUERY_KEY.PREPAYMENTS]: [],
        [QUERY_KEY.BUS_OPTIONS]: [],
        [QUERY_KEY.TICKET_BACK]: undefined,
        [QUERY_KEY.CURRENCY_ID]: undefined,
        [QUERY_KEY.PASSENGERS]: {
          children: { count: undefined, ages: [] },
          animals: { count: undefined, weights: [] },
          adults: { count: 1 },
        },
        [QUERY_KEY.ORDER_ID]: undefined,
        [QUERY_KEY.PER_PAGE]: undefined,
        [QUERY_KEY.SORT]: undefined,
        [QUERY_KEY.FROM]: { table: undefined, id: undefined },
        [QUERY_KEY.TO]: { table: undefined, id: undefined },
        [QUERY_KEY.PAGE]: 1,
      }

      // set defaults sort and currency
      if (this.filters.sortFields.length > 0) {
        this.fetchQuery[QUERY_KEY.SORT] = this.filters.sortFields[0].id
      }
      if (this.filters.currencies.length > 0) {
        const UAHCurrency = this.filters.currencies.find(
          (currency) => currency.code === "UAH"
        )

        this.fetchQuery[QUERY_KEY.CURRENCY_ID] = UAHCurrency
          ? UAHCurrency.id
          : this.filters.currencies[0].id
        // this.fetchQuery[QUERY_KEY.CURRENCY_ID] = this.filters.currencies[0].id
      }
    },
    resetSearchValidationErrors() {
      this.validationErrors = {
        city: {
          from: false,
          to: false,
        },
        departureDate: false,
        passengers: false,
      }
    },
    resetSearch() {
      this.search = {
        departureDate: "",
        fromCity: {
          searchResults: [],
          selected: null,
          query: "",
        },
        toCity: {
          searchResults: [],
          selected: null,
          query: "",
        },
        passengers: {
          adults: {
            count: 1,
          },
          children: {
            ages: [],
            count: 0,
          },
          animals: {
            weights: [],
            count: 0,
          },
          format: "",
        },
      }
    },
    resetDeparturesData() {
      this.trips = {
        dates: [
          {
            date: { date: "", name: "" },
            price: 0,
            busDeparturesCount: 0,
            active: false,
          },
          {
            date: { date: "", name: "" },
            price: 0,
            busDeparturesCount: 0,
            active: false,
          },
          {
            date: { date: "", name: "" },
            price: 0,
            busDeparturesCount: 0,
            active: false,
          },
        ],
        departures: {
          first_page_url: null,
          next_page_url: null,
          prev_page_url: null,
          current_page: 1,
          per_page: null,
          from: null,
          path: null,
          data: [],
          to: null,
        },
        nearestDates: [],
      }
    },
  },
  getters: {
    getCurrenciesNameConcat: ({ filters }) => {
      return filters.currencies.map((currency) => ({
        code: currency.code,
        id: currency.id,
        name: `${currency.name}, ${currency.code}`,
      }))
    },
    isSelectedFilter: ({ fetchQuery }) => {
      return (valueKey: FetchQueryIdT, filterKey: QUERY_KEY): boolean => {
        if (Array.isArray(fetchQuery[filterKey])) {
          return fetchQuery[filterKey]?.some(
            (filterId: number) => filterId === valueKey
          )
        } else {
          return fetchQuery[filterKey] === valueKey
        }
      }
    },
    getSelectedSort: ({ filters, fetchQuery }) => {
      return (
        filters.sortFields.find(
          (sortType) => sortType.id === fetchQuery.sort
        ) || null
      )
    },
    getSelectedCurrency: ({ filters, fetchQuery }) => {
      return filters.currencies.find(
        (currency: ICurrencyOption) => currency.id === fetchQuery.currencyId
      )
    },
    getSelectedFilters: ({ fetchQuery, filters }) => {
      const selectedFilters: IFilterPayload[] = []
      const arrayFiltersKeys = new Map([
        [QUERY_KEY.BUS_OPTIONS, "busOptions"],
        [QUERY_KEY.DEPARTURE_BUS_STOPS, "departureBusStops"],
        [QUERY_KEY.ARRIVAL_BUS_STOPS, "arrivalBusStops"],
        [QUERY_KEY.DEPARTURE_TYPE_ID, "departureTypes"],
        [QUERY_KEY.DEPARTURE_TIMES, "departureTimes"],
        [QUERY_KEY.TRANSPORT_TYPES, "transportTypes"],
        [QUERY_KEY.PREPAYMENTS, "prepayments"],
      ])

      for (const [filterKey, filterArrayName] of arrayFiltersKeys) {
        fetchQuery[filterKey].forEach((selectedId: number) => {
          const filterExist = filters[filterArrayName].find(
            (option) => option.id === selectedId
          )

          if (filterExist) {
            selectedFilters.push({
              filterName: filterExist.name,
              filterId: selectedId,
              filterKey,
            })
          }
        })
      }

      return selectedFilters
    },
    getActiveDate: ({ trips }) => {
      return trips.dates.find((date) => date.active)
    },
    getRecountedPrice: () => {
      return (price: number) => {
        return price / 100
      }
    },
    hasValidationError: ({ validationErrors }) => {
      // Проверяем каждый ключ объекта validationErrors на наличие значения true
      for (const key in validationErrors) {
        if (typeof validationErrors[key] === "object") {
          // Если значение ключа - объект, то проверяем его вложенные ключи
          for (const subKey in validationErrors[key]) {
            if (validationErrors[key][subKey] === true) {
              return true // Если есть хотя бы одна ошибка в объекте - возвращаем true
            }
          }
        } else if (validationErrors[key] === true) {
          return true // Если значение ключа равно true, возвращаем true
        }
      }

      return false // Если не найдено ни одного ключа с true, возвращаем false
    },
    tripDetailsTabHasContent: () => {
      return (data: unknown) => {
        for (const key in data) {
          const value = data[key]

          if (Array.isArray(value) && value.length) return true
          if (typeof value === "string" && value) return true
          if (typeof value === "boolean" && value) return true
        }

        return false
      }
    },
  },
})
