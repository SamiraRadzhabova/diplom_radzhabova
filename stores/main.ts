import { IMainState } from "~/types/main"

export const useMainStore = defineStore("main", {
  state: (): IMainState => ({
    // search: {},
    currentLang: "uk",
  }),
  getters: {},
  actions: {},
})
