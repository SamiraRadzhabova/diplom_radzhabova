import {
  ISelectOption,
  ICurrencyOption,
  ITripFilterOption,
  ISearchCityItem,
} from "~/types"

import { IPaginatedData, IFetchQuery } from "~/types/api"

// Constants
import { QUERY_KEY, PAYMENT } from "~/constants/trips"
import { IBookingDetails, ITripInfo, ITripRoute } from "./booking"
import { QUERY_SORT_TYPE } from "~/constants/api"

export type FetchQueryIdT = number

// The only types of the query those probably could contain multiple value =>
// therefore could be transformed into an array of values

export interface IFilterPayload {
  filterId: FetchQueryIdT
  filterKey: QUERY_KEY
  filterName: string | null
}

export interface ITripState {
  pendingTripsDetails: boolean
  pendingTripsFetch: boolean
  pendingTripsMore: boolean
  pendingSearchBar: boolean
  trips: ITripDepartures

  search: {
    departureDate: string
    fromCity: {
      searchResults: ISearchCityItem[]
      selected: ISearchCityItem | null
      query: string
    }
    toCity: {
      searchResults: ISearchCityItem[]
      selected: ISearchCityItem | null
      query: string
    }
    passengers: {
      adults: { count: number }
      children: {
        ages: { age: number }[]
        count: number
      }
      animals: {
        weights: { weight: number }[]
        count: number
      }
      format: string
    }
  }
  filters: ITripFilters
  fetchQuery: IFetchQuery
  validationErrors: ISearchValidationErrors
}

export interface ISearchValidationErrors {
  city: {
    from: boolean
    to: boolean
  }
  departureDate: boolean
  passengers: boolean
}
export interface ITripFilters {
  departureTypes: ISelectOption[]
  prepayments: ISelectOption[]
  transportTypes: ISelectOption[]
  departureTimes: ISelectOption[]
  busOptions: ITripFilterOption[]
  currencies: ICurrencyOption[]
  departureBusStops: ISelectOption[]
  arrivalBusStops: ISelectOption[]
  sortFields: ISelectOption[]
}
export interface ITripDepartures {
  dates: ITripsDeparturesDate[]
  departures: IPaginatedData<ITrip>
  nearestDates: INearestDateTrip[]
}
export interface INearestDateTrip {
  date: { date: string; name: string }
  departures: IPaginatedData<ITrip>
}
export type ITripsNearest = Pick<ITripDepartures, "departures">

export interface ITripsDeparturesDate {
  date: { date: string; name: string }
  price: number
  busDeparturesCount: number
  active: boolean
}
export interface ITripBusStop {
  id: number | null
  time: string
  cityName: string
  photo: string
  address: string
  ownAddress: boolean
}
export interface ITrip {
  id: number
  badges: ITripBadges
  duration: string
  startBusStop: ITripBusStop
  endBusStop: ITripBusStop
  icons: string[]
  carrier: {
    name: string
    rating: string
    needPassport: boolean
  }
  schedule: string
  price: number
  priceWithDiscount: number
  payment: IPayment
  more?: ITripDetails
}
export interface IPayment {
  hasPrepayment: boolean
  prepaymentPercent: number
  paymentType: PAYMENT
}
export interface ITripBadges {
  recommended: boolean
  fastest: boolean
  cheapest: boolean
  busChangesCount: number
}
export interface ITripDetails {
  info: ITripInfo
  details: IBookingDetails
  route: ITripRoute
  discounts: ITripDiscounts
  rules: ITripRules
  conditions: ITripConditions
}
export interface ITripIcon {
  icon: string
  name: string
}
export interface ITripDiscounts {
  children: ITicketPassenger[]
  animals: ITicketPassenger[]
  other: ITicketPassenger[]
}

export interface ITripRules {
  childrenTransportRules: string
  animalsAllowed: boolean | null
  animalsTransportRules: string
  maxAnimalWeight: number | null
  animalWithSeparatePlace: boolean
  animalWithSeparatePlacePrice: number | null
  animalWithoutSeparatePlace: boolean
  animalWithoutSeparatePlacePrice: number | null
  needManagerClarifyAnimal: boolean
}
export interface ITripConditions {
  return: string
}
export interface ITicketPassenger {
  id: number
  type: string
  name: string
}
export interface ITripLocation {
  countryName: string
  table: string
  name: string
  id: number
}
export interface ISEOLinkTrips {
  filter: {
    from: ISearchCityItem
    to: ISearchCityItem
    departureDate: string
    currencyId: number
    passengers: {
      adults: {
        count: 1
      }
      children: {
        count: 0
        ages: []
      }
      animals: {
        count: 0
        weights: []
      }
    }
    departureTypeIds: number[]
    transportTypeIds: number[]
    prepaymentIds: number[]
    departureTimeIds: number[]
    busOptionIds: number[]
    departureBusStopIds: number[]
    arrivalBusStopIds: number[]
    ownDepartureBusStop: boolean
    ownArrivalBusStop: boolean
    isTicketBack: boolean
    orderId: null | number
  }
  sort: QUERY_SORT_TYPE
  data: ITripDepartures
}
export type TripModalFlagsKeys = "currency" | "filter" | "price" | "sort"
export type TripModalFlags = {
  [key in TripModalFlagsKeys]: boolean
}
