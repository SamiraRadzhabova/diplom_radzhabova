export interface IOption {
  name?: string | number | null;
  value: string | number;
  info?: string;
  text?: string;
  selected?: boolean;
  id?: string | number | null;
}

export interface IPromo {
  title: string;
  text: string;
  src?: string;
  additionText?: string;
}

// base option item for app selects/filters/etc.
export interface ISelectOption {
  id: string | number;
  name: string | number;
}
export interface ICurrencyOption extends ISelectOption {
  code: string;
}
export interface ITripFilterOption extends ISelectOption {
  icon: string;
}

// search fields types
export interface ISearchCityItem {
  id: number;
  name: string;
  table: string;
  countryName: string;
}
export type SearchModalFlagsKeys =
  | "fromCity"
  | "toCity"
  | "passengers"
  | "datepicker";
export type SearchModalFlags = {
  [key in SearchModalFlagsKeys]: boolean;
};
export type ModalFlagsTuple = [keyof SearchModalFlags, boolean][];
export interface IComponentValidationErrors {
  [key: string]: string | null | undefined;
}
