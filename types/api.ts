// Constants =>
import { API_RESPONSE_STATUS } from "~/constants/api"

// Fetch error =>
export interface IFetchError {
  __nuxt_error: boolean
  statusMessage: string
  statusCode: number
  unhandled: boolean
  fatal: boolean
  data: {
    status: API_RESPONSE_STATUS.ERROR
    message?: string
    errors?: {
      [key: string]: string[]
    }
  }
}

export interface IValidationErrors {
  [key: string]: string[]
}

export interface INuxtError {
  url: string
  statusCode: number
  statusMessage: string
  message: string
  description: string
  data: unknown
}

// Fetch success =>
export interface IFetchSuccess<DataT> {
  status: API_RESPONSE_STATUS.SUCCESS
  data: DataT
}
export interface IFetchSuccessSimple {
  status: API_RESPONSE_STATUS.SUCCESS
}
export interface IFetchSuccessPaginated<DataT> {
  status: API_RESPONSE_STATUS.SUCCESS
  data: IPaginatedData<DataT>
}
export interface IPaginatedData<DataT> {
  first_page_url: string | null
  next_page_url: string | null
  prev_page_url: string | null
  current_page: number
  per_page: number | null
  from: number | null
  path: string | null
  data: DataT[]
  to: number | null
}
export interface IPaginatedSimpleData<DataT> {
  firstPageUrl: string | null
  nextPageUrl: string | null
  prevPageUrl: string | null
  currentPage: number
  perPage: number | null
  from: number | null
  path: string | null
  data: DataT[]
  to: number | null
}
export interface IFetchQuery {
  [key: string]:
    | IQueryPassengers
    | IQueryLocation
    | undefined
    | number[]
    | number
    | boolean
    | string
  departureBusStopIds: number[]
  ownDepartureBusStop: boolean | undefined
  ownArrivalBusStop: boolean | undefined
  arrivalBusStopIds: number[]
  departureTypeIds: number[]
  departureTimeIds: number[]
  transportTypeIds: number[]
  departureDate: string | undefined
  prepaymentIds: number[]
  busOptionIds: number[]
  ticketBack: boolean | undefined
  passengers: IQueryPassengers
  currencyId: number | string | undefined
  orderId: number | undefined
  perPage: number | undefined
  sort: string | number | undefined
  page: number
  from: IQueryLocation
  to: IQueryLocation
}

export interface IQueryLocation {
  table: string | undefined
  id: number | undefined
}
export interface IQueryPassengers {
  children: { count: number | undefined; ages: number[] }
  animals: { count: number | undefined; weights: number[] }
  adults: { count: number | undefined }
}

export interface IFetchResult {
  status: typeof API_RESPONSE_STATUS
}
