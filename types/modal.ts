import { VueComponent } from "~/stores/modal"

export interface IModalState {
  isShownOverlay: boolean
  isShownContent: boolean
  component: VueComponent | null
  props: object
}
export interface IModalProps {
  component: VueComponent | null
  props: object
}
