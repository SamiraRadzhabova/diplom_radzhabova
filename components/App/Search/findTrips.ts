import { LocationQueryRaw } from "#vue-router"
import { tripsStore } from "./SearchForm.vue"

// Form submission =>
export async function findTrips() {
  const { data: tripsFetched } = await tripsStore.fetchTrips()

  if (tripsFetched) {
    const currentQuery = JSON.parse(JSON.stringify(fetchQuery.value))

    return navigateTo({
      // path: "/trips",
      params: {
        buses: "автобуси",
        from: search.value.fromCity.selected?.name,
        to: search.value.toCity.selected?.name,
      },
      query: currentQuery as LocationQueryRaw,
    })
  }
}
