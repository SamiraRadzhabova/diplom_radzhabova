export const TRIP_TAB_EMPTY_LAYOUT = {
  img: "/assets/img/trip/trip-empty-more.svg",
  title: "Інформація відстуня",
  text: "Будь ласка, зверніться до нашого менеджера, щоб дізнатись додаткову інформацію.",
}

export enum QUERY_KEY {
  DEPARTURE_BUS_STOPS = "departureBusStopIds",
  DEPARTURE_OWN_STOP = "ownDepartureBusStop",
  ARRIVAL_BUS_STOPS = "arrivalBusStopIds",
  DEPARTURE_TYPE_ID = "departureTypeIds",
  ARRIVAL_OWN_STOP = "ownArrivalBusStop",
  TRANSPORT_TYPES = "transportTypeIds",
  DEPARTURE_TIMES = "departureTimeIds",
  DEPARTURE_DATE = "departureDate",
  PREPAYMENTS = "prepaymentIds",
  BUS_OPTIONS = "busOptionIds",
  TICKET_BACK = "ticketBack",
  CURRENCY_ID = "currencyId",
  PASSENGERS = "passengers",
  ORDER_ID = "orderId",
  PER_PAGE = "perPage",
  SORT = "sort",
  PAGE = "page",
  FROM = "from",
  TO = "to",
}
export const enum PAYMENT {
  PARTIAL = "PARTIAL_PREPAYMENT",
  FULL = "FULL_PREPAYMENT",
  FREE = "FREE_BOOKING",
}
