export const enum API_RESPONSE_STATUS {
  SUCCESS = "success",
  ERROR = "error",
}

export const enum API_ERRORS {
  UNAUTHORIZED = 401, // user hadn't logged up for a request
  FORBIDDEN = 403, // user has no permissions for a request
  NOT_FOUND = 404, // a request returned no result
  UNPROCESSABLE_ENTITY = 422, // validation errors
  TOO_MANY_REQUESTS = 429, // user had requested too often for a moment of time
}

// Fetch query sort type =>
export const enum QUERY_SORT_TYPE {
  DEP_TIME_ASC = "departureTimeAscending",
  BUS_TIME_ASC = "busStopTimeAscending",
  ARR_TIME_ASC = "arrivalTimeAscending",
  RATE_DESC = "ratingDescending",
  ASC = "priceAscending",
  DESC = "priceDescending",
}

export const enum PROMISE_RESULT {
  SUCCESS = "fulfilled",
  REJECT = "rejected",
}
