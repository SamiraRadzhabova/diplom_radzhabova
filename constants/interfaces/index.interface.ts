export interface QUESTION_I {
  TITLE: string
  ANSWERS: {
    BODY: string
    HEAD: string
  }[]
}

export interface ITab {
  tabName: string
  value: string
}

export interface REVIEW_I {
  ID: number
  COMMENT: string[]
  FULLNAME: string
  INFO: string
}

export interface ADVANTAGE_ITEMS_I {
  TITLE: string
  DESC: string
  BACKGROUND: string
  IMG_SRC: string
}

export interface FOOTER_I {
  TITLE: string
  INFO: {
    NAME: string
    LINK: string
  }[]
  ALL?: {
    NAME: string
    LINK: string
  }
  ARROW: boolean
  CLASS?: string
}

export interface FOOTER_SITE_I {
  TITLE: string
  INFO: {
    NAME: string
    LINK: string
  }[]
}
export interface FOOTER_SOCIAL_I {
  LOGO: string
}

export interface HEADER_I {
  NAV: {
    ICON: string
    ALT: string
  }[]
}

export interface PARTNER_I {
  LOGO: string
  BACKGROUND: string
  PADDING: string
}

export interface ISLUG_GALLERY {
  SRC: string
  ID: number
}
export interface PRIVACY_POLICY_I {
  TITLE?: string
  INFO: string[]
}

export interface PUBLIC_OFFER_I {
  TITLE: string
  BODY: {
    TEXT: string
    PARAGRAPH?: string[]
    ADDITIONAL?: string
  }[]
}

export interface FACILITY_CARD_I {
  TITLE: string
  TEXT: string
  NAME: string
}

export interface CONTACTS_I {
  TITLE: string
  INFO: {
    ICON: string
    NUMBER: string
  }[]
}

export interface ADDRESS_I {
  CITY: string
  STREET: string
  SRC: string
}

export interface REASON_I {
  SUBTITLE: string
  TITLE: string
  TEXT: string
  SRCM: string
  SRC: string
}

export interface ARTICLE_I {
  DATE: string
  TITLE: string
  TEXT: string
  SRC: string
}

export interface SLUG_I {
  TEXT?: string
  TITLE?: string
  NOTE?: string
  CONTENT?: {
    TEXT?: string
    TITLE?: string
    CHOICE?: string
    LIST?: string[]
    CLASS?: string
  }[]
}

export interface COMMENT_I {
  ID: number
  NAME: string
  DATE: string
  COMMENT: string
  COUNT?: number
  ANSWERS?: (COMMENT_I & { ANSWER_AT: { NICK: string; ID: number } })[]
}

export interface CONFIRMED_I {
  ICON: string
  TITLE: string
  BODY: {
    TEXT?: string
    NOTICE?: {
      TITLE?: string
      TEXT?: string
      LINK?: string
    }
    BLOCK?: {
      TITLE?: string
      TEXT?: string
    }
    LIST?: string[]
  }
}
export interface IPERSONAL_CABINET_TABS {
  NAME: string
  ROUTE: string
}
