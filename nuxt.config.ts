// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  runtimeConfig: {
    public: {
      API_BASE_URL:
        process.env.NUXT_API_BASE_URL || "https://api.rubikon.staj.bid/api/v1/",
    },
  },
  css: ["@/assets/scss/main.scss"],
  vite: {
    vue: {
      script: {
        propsDestructure: true,
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `
            @import "@/assets/scss/util";
            @import "@/assets/scss/vars.scss";
          `,
        },
      },
    },
  },
  build: {
    transpile: ["@vuepic/vue-datepicker"],
  },
  modules: [
    "@nuxtjs/eslint-module",
    "@pinia/nuxt",
    "@pinia-plugin-persistedstate/nuxt",
    "@nuxt/image",
    "@vueuse/nuxt",
    "nuxt-lodash",
    "nuxt-icon",
    "nuxt-delay-hydration",
  ],
  // pinia: {
  //   autoImports: ["defineStore", "definePiniaStore"],
  // },
  postcss: {
    plugins: {
      autoprefixer: {
        overrideBrowserslist: ["last 4 versions"],
        cascade: false,
      },
    },
  },
  eslint: {
    lintOnStart: false,
  },
  image: {
    inject: true,
  },
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  delayHydration: {
    debug: true,
    mode: "mount",
  },
  typescript: {
    tsConfig: {
      compilerOptions: {
        verbatimModuleSyntax: false,
      },
    },
  },
})
